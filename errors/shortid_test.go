package errors_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/gaia-x/data-infrastructure-federation-services/tsa/golib/errors"
)

func TestNewID(t *testing.T) {
	for i := 0; i < 100; i++ {
		id := errors.NewID()
		assert.Len(t, id, 16)

		for _, r := range id {
			assert.Contains(t, errors.Alphabet, string(r))
		}
	}
}
